import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TeamtableComponent } from './teamtable/teamtable.component';



const routes: Routes = [
  { path: '', component: TeamtableComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
