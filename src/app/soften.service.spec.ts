import { TestBed } from '@angular/core/testing';

import { SoftenService } from './soften.service';

describe('SoftenService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SoftenService = TestBed.get(SoftenService);
    expect(service).toBeTruthy();
  });
});
