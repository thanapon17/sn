
import {
  Component,
  OnInit,
} from "@angular/core";

import { HttpClient } from '@angular/common/http';
import { SoftenService } from '../soften.service';
import { MatTableDataSource } from "@angular/material/table";


export interface lastmatch {
  date: string;
  team_home: string;
  score: string;
  team_away: string;
}

export interface match {
  date: string;
  team_home: string;
  score: string;
  team_away: string;
}

export interface typeM {
  type: string;
  homeresult: string;
  awayresult: string;
}

export interface Team_Name {
  value: string;
  viewValue: string;
}

export interface staticData {
  shots: number;
  sOnTarget: number;
  sOutTarget: number;
  fouls: number;
  corners: number;
}

export interface avgData {
  team: string;
  HGS: number;
  AGS: number;
  HAS: number;
  AAS: number;
  HGC: number;
  AGC: number;
  HDS: number;
  ADS: number;
}

@Component({
  selector: 'app-teamtable',
  templateUrl: './teamtable.component.html',
  styleUrls: ['./teamtable.component.scss']
})


export class TeamtableComponent implements OnInit {



  homeTeam: string = '../../assets/photo/Premier-League-Logo.png';
  awayTeam: string = '../../assets/photo/Premier-League-Logo.png';

  winrateH: number = 0;
  winrateA: number = 0;
  draw: number = 0;

  last_match = [];
  lastH_matcheach = [];
  lastA_matcheach = [];
  match_details = [];

  shotsH: number = 0;
  sOnTargetH: number = 0;
  yallowCardH: number = 0;
  redCardH: number = 0;
  foulsH: number = 0
  cornersH: number = 0;


  shotsA: number = 0;
  sOnTargetA: number = 0;
  yallowCardA: number = 0;
  redCardA: number = 0;
  foulsA: number = 0
  cornersA: number = 0;

  modal_team: string;
  modal_HGS: number = 0;
  modal_AGS: number = 0;
  modal_HAS: number = 0;
  modal_AAS: number = 0;
  modal_HGC: number = 0;
  modal_AGC: number = 0;
  modal_HDS: number = 0;
  modal_ADS: number = 0;


  displayedColumns0: string[] = ['date', 'team_home', 'score', 'team_away'];
  dataSource0: any = []

  displayedColumns1: string[] = ['date', 'team_home', 'score', 'team_away'];
  dataSource1

  displayedColumns2: string[] = ['date', 'team_home', 'score', 'team_away'];
  dataSource2

  displayedColumns3: string[] = ['modal_team', 'modal_HGS', 'modal_AGS', 'modal_HAS', 'modal_AAS', 'modal_HGC', 'modal_AGC', 'modal_HDS', 'modal_ADS'];
  dataSource3

  constructor(private http: HttpClient, private soften: SoftenService) { }

  public barChartOptions = {
    scaleShowVerticalLines: false,
    responsive: true
  };
  public barChartLabels = [];
  public barChartType = 'horizontalBar';
  public barChartLegend = true;
  public barChartData = [
    { data: [], label: 'เป็นทีมเหย้าทำประตู' },// HGS
    { data: [], label: 'เป็นทีมเยือนทำประตู', hidden: true },// AGS
    { data: [], label: 'เกมรุกทีมเหย้า', hidden: true },// HAS
    { data: [], label: 'เกมรุกทีมเยือน', hidden: true },// AAS
    { data: [], label: 'เป็นทีมเหย้าโดนทำประตู', hidden: true },// HGC
    { data: [], label: 'เป้นทีมเยือนโดนทำประตู', hidden: true },// AGC
    { data: [], label: 'เกมรับทีมเหย้า', hidden: true },// HDS
    { data: [], label: 'เกมรับทีมเยือน', hidden: true },// ADS

  ];

  ngOnInit() {

  }

  deteam: any
  lastmatch: any
  lastestmatcheach: any
  performance: any
  matchdetails: any
  matchdetailsChart: any

  teams: Team_Name[] = [
    { viewValue: 'Arsenal', value: '../../assets/photo/Arsenal.png' },
    { viewValue: 'Aston Villa', value: '../../assets/photo/Aston Villa.png' },
    { viewValue: 'Birmingham', value: '../../assets/photo/Birmingham City.png' },
    { viewValue: 'Blackburn', value: '../../assets/photo/Blackburn Rovers.png' },
    { viewValue: 'Blackpool', value: '../../assets/photo/Blackpool.png' },
    { viewValue: 'Bolton', value: '../../assets/photo/Bolton Wanderers.png' },
    { viewValue: 'Bournemouth', value: '../../assets/photo/AFC Bournemouth.png' },
    { viewValue: 'Brighton', value: '../../assets/photo/Brighton & Hove Albion.png' },
    { viewValue: 'Burnley', value: '../../assets/photo/Burnley.png' },
    { viewValue: 'Cardiff', value: '../../assets/photo/Cardiff City.png' },
    { viewValue: 'Chelsea', value: '../../assets/photo/Chelsea.png' },
    { viewValue: 'Crystal Palace', value: '../../assets/photo/Crystal Palace.png' },
    { viewValue: 'Everton', value: '../../assets/photo/Everton.png' },
    { viewValue: 'Fulham', value: '../../assets/photo/Fulham.png' },
    { viewValue: 'Huddersfield', value: '../../assets/photo/Huddersfield Town.png' },
    { viewValue: 'Hull', value: '../../assets/photo/Hull City.png' },
    { viewValue: 'Leicester', value: '../../assets/photo/Leicester City.png' },
    { viewValue: 'Liverpool', value: '../../assets/photo/Liverpool.png' },
    { viewValue: 'Man City', value: '../../assets/photo/Manchester City.png' },
    { viewValue: 'Man United', value: '../../assets/photo/Manchester United.png' },
    { viewValue: 'Middlesbrough', value: '../../assets/photo/Middlesbrough.png' },
    { viewValue: 'Newcastle', value: '../../assets/photo/Newcastle United.png' },
    { viewValue: 'Norwich', value: '../../assets/photo/Norwich City.png' },
    { viewValue: 'Portsmouth', value: '../../assets/photo/Portsmouth.png' },
    { viewValue: 'QPR', value: '../../assets/photo/Queens Park Rangers.png' },
    { viewValue: 'Reading', value: '../../assets/photo/Reading.png' },
    { viewValue: 'Southampton', value: '../../assets/photo/Southampton.png' },
    { viewValue: 'Stoke', value: '../../assets/photo/Stoke City.png' },
    { viewValue: 'Sunderland', value: '../../assets/photo/Sunderland.png' },
    { viewValue: 'Swansea', value: '../../assets/photo/Swansea City.png' },
    { viewValue: 'Tottenham', value: '../../assets/photo/Tottenham Hotspur.png' },
    { viewValue: 'Watford', value: '../../assets/photo/Watford.png' },
    { viewValue: 'West Brom', value: '../../assets/photo/West Bromwich Albion.png' },
    { viewValue: 'West Ham', value: '../../assets/photo/West Ham United.png' },
    { viewValue: 'Wigan', value: '../../assets/photo/Wigan Athletic.png' },
    { viewValue: 'Wolves', value: '../../assets/photo/Wolverhampton Wanderers.png' },
  ];

  homename: string = null;
  awayname: string = null;
  new_winH: number = 0;
  new_winA: number = 0;
  new_draw: number = 0;

  new_shotsH: number = 0;
  new_sOnTargetH: number = 0;
  new_yallowCardH: number = 0;
  new_redCardH: number = 0;
  new_foulsH: number = 0;
  new_cornersH: number = 0;

  new_shotsA: number = 0;
  new_sOnTargetA: number = 0;
  new_yallowCardA: number = 0;
  new_redCardA: number = 0;
  new_foulsA: number = 0;
  new_cornersA: number = 0;

  spinner_process: boolean = true;
  temp: any = ['score1', 'score2', 'score3', 'score4', 'score5']

  numberSort = (num1: number, num2: number) => {
    return num1 - num2;
  };

  checkprocess() {
    this.spinner_process = !this.spinner_process
  }
  EventHome() {

    this.teams.map(item => {
      if (item.value == this.homeTeam) {
        console.log(item.viewValue.replace(" ", "_"))
        this.homename = item.viewValue.replace(" ", "_")
      }

    })

    this.httpRequest()
  }

  EventAway() {

    this.teams.map(item => {
      if (item.value == this.awayTeam) {
        console.log(item.viewValue.replace(" ", "_"))
        this.awayname = item.viewValue.replace(" ", "_")
      }
    })

    this.httpRequest()

  }

  httpRequest() {

    if (this.homename != null && this.awayname != null) {
      if (this.homename != this.awayname) {
        this.spinner_process = false;
        new Promise((resolve, reject) => {

          this.soften.getDeTeam(this.homename, this.awayname)
            .toPromise()
            .then(
              res => {
                console.log(this.homename)
                this.winrateH = res['propwin']['team_home'].toFixed(0)
                this.winrateA = res['propwin']['team_away'].toFixed(0)
                this.draw = res['propwin']['draw'].toFixed(0)



                //this performance
                this.shotsH = res['performance']['home'].HS_P_AS.toFixed(1)
                this.sOnTargetH = res['performance']['home'].HST_P_AST.toFixed(1)
                this.yallowCardH = res['performance']['home'].HY_P_AY.toFixed(1)
                this.redCardH = res['performance']['home'].HR_P_AR.toFixed(1)
                this.foulsH = res['performance']['home'].HF_P_AF.toFixed(1)
                this.cornersH = res['performance']['home'].HC_P_AC.toFixed(1)

                // 'ADS': parseFloat(res['performance'][index].ADS).toFixed(1).toString()

                this.shotsA = res['performance']['away'].HS_P_AS.toFixed(1)
                this.sOnTargetA = res['performance']['away'].HST_P_AST.toFixed(1)
                this.yallowCardA = res['performance']['away'].HY_P_AY.toFixed(1)
                this.redCardA = res['performance']['away'].HR_P_AR.toFixed(1)
                this.foulsA = res['performance']['away'].HF_P_AF.toFixed(1)
                this.cornersA = res['performance']['away'].HC_P_AC.toFixed(1)

                // 'ADS': parseFloat(res[index].ADS).toFixed(2).toString()



                this.graphfix()
                //this lastest match each
                this.lastH_matcheach = []

                this.lastA_matcheach = []

                for (let index in res['lastestmatcheach']['home']['data']) {
                  // console.log(res['lastestmatcheach']['home']['data'][index])
                  this.lastH_matcheach.push({
                    date: res['lastestmatcheach']['home']['data'][index].date,
                    team_home: res['lastestmatcheach']['home']['data'][index].hometeam,
                    score: res['lastestmatcheach']['home']['data'][index].homescore + '-' + res['lastestmatcheach']['home']['data'][index].awayscore,
                    team_away: res['lastestmatcheach']['home']['data'][index].awayteam
                  })
                }

                for (let index in res['lastestmatcheach']['away']['data']) {
                  // console.log(res['lastestmatcheach']['home']['data'][index])
                  this.lastA_matcheach.push({
                    date: res['lastestmatcheach']['away']['data'][index].date,
                    team_home: res['lastestmatcheach']['away']['data'][index].hometeam,
                    score: res['lastestmatcheach']['away']['data'][index].homescore + '-' + res['lastestmatcheach']['away']['data'][index].awayscore,
                    team_away: res['lastestmatcheach']['away']['data'][index].awayteam
                  })
                }
                // console.log(this.lastH_matcheach)
                this.dataSource1 = new MatTableDataSource();
                this.dataSource1.data = this.lastH_matcheach;

                this.dataSource2 = new MatTableDataSource();
                this.dataSource2.data = this.lastA_matcheach;


                //this lastest match
                this.last_match = []

                for (let index in res['lastestmatch']['data']) {
                  this.last_match.push({
                    date: res['lastestmatch']['data'][index].date,
                    team_home: res['lastestmatch']['data'][index].hometeam,
                    score: res['lastestmatch']['data'][index].homescore + '-' + res['lastestmatch']['data'][index].awayscore,
                    team_away: res['lastestmatch']['data'][index].awayteam
                  })
                }

                this.dataSource0 = new MatTableDataSource();
                this.dataSource0.data = this.last_match;
                resolve();
              }
            );
        }).then(() => this.spinner_process = true);
      }
      else {
        this.spinner_process = true

        this.winrateH = 0
        this.winrateA = 0
        this.draw = 0

        this.dataSource0 = new MatTableDataSource();
        this.dataSource0.data = this.last_match = [];
        this.dataSource1 = new MatTableDataSource();
        this.dataSource1.data = this.lastH_matcheach = [];
        this.dataSource2 = new MatTableDataSource();
        this.dataSource2.data = this.lastA_matcheach = [];

        this.shotsH = 0
        this.sOnTargetH = 0
        this.yallowCardH = 0
        this.redCardH = 0
        this.foulsH = 0
        this.cornersH = 0

        this.shotsA = 0
        this.sOnTargetA = 0
        this.yallowCardA = 0
        this.redCardA = 0
        this.foulsA = 0
        this.cornersA = 0

        this.graphfix()
      }

    }

  }
  columnDefs = [

    { headerName: 'Team', field: 'Team', sortable: true, filter: true, width: 120 },
    { headerName: 'เป็นทีมเหย้าทำประตู', field: 'HGS', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เป็นทีมเยือนทำประตู', field: 'AGS', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เกมรุกทีมเหย้า', field: 'HAS', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เกมรุกทีมเยือน', field: 'AAS', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เป็นทีมเหย้าโดนทำประตู', field: 'HGC', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เป็นทีมเยือนโดนทำประตู', field: 'AGC', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เกมรับทีมเหย้า', field: 'HDS', sortable: true, filter: true, comparator: this.numberSort },
    { headerName: 'เกมรับทีมเยือน', field: 'ADS', sortable: true, filter: true, comparator: this.numberSort },
  ];

  rowData: any;

  graphfix() {
    if (this.winrateH <= 10 && this.winrateH > 0) {
      this.new_winH = 10
    }
    else {
      this.new_winH = this.winrateH
    }

    if (this.winrateA <= 10 && this.winrateA > 0) {
      this.new_winA = 10
    }
    else {
      this.new_winA = this.winrateA
    }

    if (this.draw <= 10 && this.draw > 0) {
      this.new_draw = 10
    }
    else {
      this.new_draw = this.draw
    }

    if (this.shotsH <= 10 && this.shotsH > 0) {
      this.new_shotsH = 10
    }
    else {
      this.new_shotsH = this.shotsH
    }

    if (this.sOnTargetH <= 10 && this.sOnTargetH > 0) {
      this.new_sOnTargetH = 10
    }
    else {
      this.new_sOnTargetH = this.sOnTargetH
    }

    if (this.yallowCardH <= 10 && this.yallowCardH > 0) {
      this.new_yallowCardH = 10
    }
    else {
      this.new_yallowCardH = this.yallowCardH
    }

    if (this.redCardH <= 10 && this.redCardH > 0) {
      this.new_redCardH = 10
    }
    else {
      this.new_redCardH = this.redCardH
    }

    if (this.foulsH <= 10 && this.foulsH > 0) {
      this.new_foulsH = 10
    }
    else {
      this.new_foulsH = this.foulsH
    }

    if (this.cornersH <= 10 && this.cornersH > 0) {
      this.new_cornersH = 10
    }
    else {
      this.new_cornersH = this.cornersH
    }

    if (this.shotsA <= 10 && this.shotsA > 0) {
      this.new_shotsA = 10
    }
    else {
      this.new_shotsA = this.shotsA
    }

    if (this.sOnTargetA <= 10 && this.sOnTargetA > 0) {
      this.new_sOnTargetA = 10
    }
    else {
      this.new_sOnTargetA = this.sOnTargetA
    }

    if (this.yallowCardA <= 10 && this.yallowCardA > 0) {
      this.new_yallowCardA = 10
    }
    else {
      this.new_yallowCardA = this.yallowCardA
    }

    if (this.redCardA <= 10 && this.redCardA > 0) {
      this.new_redCardA = 10
    }
    else {
      this.new_redCardA = this.redCardA
    }
    if (this.foulsA <= 10 && this.foulsA > 0) {
      this.new_foulsA = 10
    }
    else {
      this.new_foulsA = this.foulsA
    }

    if (this.cornersA <= 10 && this.cornersA > 0) {
      this.new_cornersA = 10
    }
    else {
      this.new_cornersA = this.cornersA
    }
  }
  ClickModal() {

    this.rowData = new Promise((resolve, reject) => {

      this.soften.getmatchdetails()
        .toPromise()
        .then(
          res => {
            // console.log(res);
            this.matchdetails = res;
            this.match_details = []


            for (let index in res) {
              // console.log(res[index].Team)
              // console.log(res['home']['data'][index])
              this.match_details.push(
                {
                  'Team': res[index].Team,
                  'HGS': parseFloat(res[index].HGS).toFixed(2),
                  'AGS': parseFloat(res[index].AGS).toFixed(2),
                  'HAS': parseFloat(res[index].HAS).toFixed(2),
                  'AAS': parseFloat(res[index].AAS).toFixed(2),
                  'HGC': parseFloat(res[index].HGC).toFixed(2),
                  'AGC': parseFloat(res[index].AGC).toFixed(2),
                  'HDS': parseFloat(res[index].HDS).toFixed(2),
                  'ADS': parseFloat(res[index].ADS).toFixed(2)
                }

              )
            }

            resolve(this.match_details);

          }
        );
    });


  }
  TEAMNAME = []; AAS = []; ADS = []; AGC = []; AGS = []; 
  HAS = []; HDS = []; HGC = []; HGS = [];
  dataChart() {
    this.TEAMNAME = []; this.AAS = []; this.ADS = []; this.AGC = []; 
    this.AGS = [];  this.HAS = []; this.HDS = []; this.HGC = []; this.HGS = [];

    this.soften.getmatchdetails().subscribe(res => {
      

      // console.log(res)
      for (let index in res) {
        this.TEAMNAME.push(res[index].Team)
        this.HGS.push(res[index].HGS)
        this.AGS.push(res[index].AGS)
        this.HAS.push(res[index].HAS)
        this.AAS.push(res[index].AAS)
        this.HGC.push(res[index].HGC)
        this.AGC.push(res[index].AGC)
        this.HDS.push(res[index].HDS)
        this.ADS.push(res[index].ADS)

      }

      this.barChartLabels = this.TEAMNAME
      this.barChartData[0]['data'] = this.HGS
      this.barChartData[1]['data'] = this.AGS
      this.barChartData[2]['data'] = this.HAS
      this.barChartData[3]['data'] = this.AAS
      this.barChartData[4]['data'] = this.HGC
      this.barChartData[5]['data'] = this.AGC
      this.barChartData[6]['data'] = this.HDS
      this.barChartData[7]['data'] = this.ADS

    })


  }



}

