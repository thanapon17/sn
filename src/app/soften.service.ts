import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable({
  providedIn: 'root'
})
export class SoftenService {
  propwin_url = 'http://178.128.111.212:6968/propwin';
  // lastestmatch_url = 'http://178.128.111.212/lastestmatch';
  // lastestmatcheach_url = 'http://178.128.111.212/lastestmatcheach';
  // performance_url = 'http://178.128.111.212/performance';
  matchdetails_url = 'http://178.128.111.212:6968/matchdetails';

  constructor(private http : HttpClient) {

  }

  getDeTeam(home, away){
    console.log(this.propwin_url + `?home=${home}&away=${away}`)
     return this.http.get(this.propwin_url + `?home=${home}&away=${away}`);
  }

  getmatchdetails(){
    console.log(this.matchdetails_url)
    return this.http.get(this.matchdetails_url);
  }
}
